package me.abe.intensiveheights.util;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class ItemStackUtil {

    public static String serialize(ItemStack stack) {
        String type = stack.getType().toString();
        String enches = "";
        Map<Enchantment, Integer> enchantments = stack.getEnchantments();
        for(Enchantment ench : enchantments.keySet())
            enches += ench.getName() + ":" + enchantments.get(ench) + ", ";
        if(enches.endsWith(", "))
            enches = enches.substring(0, enches.length() - 2);
        if(enches.equals(""))
            return type + ", " + stack.getDurability();
        else
            return type + ", " + stack.getDurability() + ", " + enches;
    }

    public static ItemStack deserialize(String raw) {
        String[] rawData = raw.split(", ");
        Material type = Material.valueOf(rawData[0]);
        short data = Short.valueOf(rawData[1]);
        Map<Enchantment, Integer> enchantments = new HashMap<>();
        for(int i = 2; i < rawData.length; i++) {
            String[] enchRaw = rawData[i].split(":");
            String name = enchRaw[0];
            String amt = enchRaw[1];
            enchantments.put(Enchantment.getByName(name), Integer.valueOf(amt));
        }
        ItemStack item = new ItemStack(type);
        item.setDurability(data);
        item.addUnsafeEnchantments(enchantments);
        return item;
    }

}
