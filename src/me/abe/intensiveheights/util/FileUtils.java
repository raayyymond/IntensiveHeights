package me.abe.intensiveheights.util;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileUtils {

    private static String DIRECTORY = "plugins/IntensiveHeights/";
    public static FileConfiguration getFile(String fileName) {
        return YamlConfiguration.loadConfiguration(getFile(fileName, true));
    }
    public static void saveFile(FileConfiguration file, String name) {
        try {
            file.save(getFile(name, false));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static File getFile(String fileName, boolean createIfNotExist) {
        try {
            File dir = new File(DIRECTORY);
            if(!dir.exists())
                dir.mkdir();
            File file = new File(DIRECTORY, fileName);
            if (createIfNotExist && !file.exists()) {
                boolean success = file.createNewFile();
                if (!success)
                    throw new IOException("File couldn't be created");
            }
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
