package me.abe.intensiveheights.util;

import org.bukkit.ChatColor;

public class Utils {

    public static String color(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public static String uncolor(String s) {
        return s.replace('§', '&');
    }

}
