package me.abe.intensiveheights.util;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

public class LocationUtil {

    public static String serialize(Location location) {
        return location.getX() + "_" + location.getY() + "_" + location.getZ() + "_" + location.getWorld().getName();
    }

    public static Location deserialize(String raw) {
        String[] rawData = raw.split("_");
        double x = Double.valueOf(rawData[0]);
        double y = Double.valueOf(rawData[1]);
        double z = Double.valueOf(rawData[2]);
        World world = new WorldCreator(rawData[3]).createWorld();
        return new Location(world, x, y, z);
    }

    public static String serializePlayer(Location location) {
        return location.getX() + "_" + location.getY() + "_" + location.getZ() + "_" + location.getWorld().getName() + "_" + location.getYaw() + "_" + location.getPitch();
    }

    public static Location deserializePlayer(String raw) {
        String[] rawData = raw.split("_");
        double x = Double.valueOf(rawData[0]);
        double y = Double.valueOf(rawData[1]);
        double z = Double.valueOf(rawData[2]);
        World world = new WorldCreator(rawData[3]).createWorld();
        float yaw = Float.valueOf(rawData[4]);
        float pitch = Float.valueOf(rawData[5]);
        return new Location(world, x, y, z, yaw, pitch);
    }

}
