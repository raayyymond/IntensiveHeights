package me.abe.intensiveheights.token;

import me.abe.intensiveheights.util.FileUtils;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TokenManager {
    private Map<UUID, Integer> economy;
    public TokenManager() {
        loadEconomy();
    }
    private void loadEconomy() {
        economy = new HashMap<>();
        FileConfiguration ecoConfig = FileUtils.getFile("tokens.yml");
        for(String rawUUID : ecoConfig.getConfigurationSection("").getKeys(false)) {
            UUID uuid = UUID.fromString(rawUUID);
            economy.put(uuid, ecoConfig.getInt(rawUUID));
        }
    }
    public boolean hasEnough(UUID uuid, int amount) {
        return getBalance(uuid) >= amount;
    }

    public void withdraw(UUID uuid, int amount) {
        economy.put(uuid, getBalance(uuid) - amount);
        updateFile();
    }
    public void deposit(UUID uuid, int amount) {
        economy.put(uuid, getBalance(uuid) + amount);
        updateFile();
    }
    public void setBalance(UUID uuid, int amount) {
        economy.put(uuid, amount);
        updateFile();
    }
    public int getBalance(UUID uuid) {
        if(!hasAccount(uuid))
            economy.put(uuid, 0);
        updateFile();
        return economy.get(uuid);
    }
    public boolean hasAccount(UUID uuid) {
        return economy.containsKey(uuid);
    }
    public void updateFile() {
        FileConfiguration ecoConfig = FileUtils.getFile("tokens.yml");
        for(UUID uuid : economy.keySet())
            ecoConfig.set(uuid.toString(), economy.get(uuid));
        FileUtils.saveFile(ecoConfig, "tokens.yml");
    }
}
