package me.abe.intensiveheights.token;

import me.abe.intensiveheights.IntensiveHeights;
import me.abe.intensiveheights.msg.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TokenCmd implements CommandExecutor {

    private IntensiveHeights plugin;

    public TokenCmd() {
        plugin = IntensiveHeights.getInstance();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        if(args.length == 0) {
            if(!(sender instanceof Player)) {
                sender.sendMessage(Message.MUST_BE_PLAYER.getMsg());
                return false;
            }

            Player player = (Player) sender;
            player.sendMessage(Message.TOKEN_BALANCE.getMsg().replace("%bal%", "" + plugin.getTokenManager().getBalance(player.getUniqueId())));
        } else if(args.length == 1) {
            Player otherPlayer = Bukkit.getPlayer(args[0]);

            if(otherPlayer == null) {
                sender.sendMessage(Message.NOT_ONLINE.getMsg());
                return false;
            }

            sender.sendMessage(Message.OTHER_TOKEN_BLANACE.getMsg().replace("%bal%", "" + plugin.getTokenManager().getBalance(otherPlayer.getUniqueId())).replace("%player%", otherPlayer.getName()));
        } else if(args.length == 2) {
            if(sender.hasPermission("ih.token.admin")) {
                Player otherPlayer = Bukkit.getPlayer(args[0]);

                if(otherPlayer == null) {
                    sender.sendMessage(Message.NOT_ONLINE.getMsg());
                    return false;
                }

                int tokens = 0;

                try {
                    tokens = Integer.valueOf(args[1]);
                } catch (NumberFormatException e) {
                    sender.sendMessage(Message.INVALID_NUMBER.getMsg());
                    return false;
                }

                plugin.getTokenManager().setBalance(otherPlayer.getUniqueId(), tokens);
            } else {
                sender.sendMessage(Message.TOKEN_CMD_HELP.getMsg());
            }
        } else {
            sender.sendMessage(Message.TOKEN_CMD_HELP.getMsg());
        }

        return true;
    }

}
