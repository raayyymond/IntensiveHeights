package me.abe.intensiveheights.sign;

public enum SignType {

    JOIN_MOB_UNDER15("[JMBU15]"),
    JOIN_MOB_OVER15("[JMBO15]"),
    JOIN_PLAYER("[JPLYR]");

    String signMsg;

    SignType(String signMsg) {
        this.signMsg = signMsg;
    }

    public String getSignMsg() {
        return signMsg;
    }

    static SignType getType(String signMsg) {
        for(SignType type : SignType.values())
            if(type.getSignMsg().equals(signMsg))
                return type;
        return null;
    }

}
