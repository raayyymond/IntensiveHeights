package me.abe.intensiveheights.sign;

import me.abe.intensiveheights.IntensiveHeights;
import me.abe.intensiveheights.match.MatchType;
import me.abe.intensiveheights.msg.Message;
import me.abe.intensiveheights.util.FileUtils;
import me.abe.intensiveheights.util.LocationUtil;
import me.abe.intensiveheights.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SignListener implements Listener {
    
    private IntensiveHeights plugin;
    
    public SignListener() {
        plugin = IntensiveHeights.getInstance();
        Bukkit.getPluginManager().registerEvents(this, plugin);
        loadDefaultConfig();
    }

    private void loadDefaultConfig() {
        for(SignType type : SignType.values()) {
            if(!plugin.getConfig().isSet("signs." + type.toString() + ".lines")) {
                List<String> lines = new ArrayList<>();
                switch (type) {
                    case JOIN_MOB_UNDER15:
                        lines.add("&7[&aJoin&7]");
                        lines.add("&c&lMob");
                        lines.add("");
                        lines.add("&bLevel < 15");
                        break;
                    case JOIN_MOB_OVER15:
                        lines.add("&7[&aJoin&7]");
                        lines.add("&c&lMob");
                        lines.add("");
                        lines.add("&bLevel > 15");
                        break;
                    case JOIN_PLAYER:
                        lines.add("&7[&aJoin&7]");
                        lines.add("&c&lPlayer");
                        lines.add("");
                        lines.add("");
                        break;
                }
                plugin.getConfig().set("signs." + type.toString() + ".lines", lines);
            }
        }
        plugin.saveConfig();
    }

    @EventHandler
    public void onCreate(SignChangeEvent e) {
        String[] lines = e.getLines();
        Player player = e.getPlayer();

        if(!player.hasPermission("ih.sign.admin"))
            return;

        SignType signType = SignType.getType(lines[0]);

        if(signType == null)
            return;

        List<String> rawLines = plugin.getConfig().getStringList("signs." + signType.toString() + ".lines");
        lines = rawLines.toArray(new String[rawLines.size()]);

        for(int i = 0; i < rawLines.size(); i++)
            e.setLine(i, Utils.color(lines[i]));

        player.sendMessage(Message.SIGN_CREATED.getMsg());

        FileConfiguration signsConfig = FileUtils.getFile("signs.yml");
        String stringLoc = LocationUtil.serialize(e.getBlock().getLocation());
        List<String> signs = signsConfig.isSet("signs") ? signsConfig.getStringList("signs") : new ArrayList<>();
        signs.add(stringLoc);
        signsConfig.set("signs", signs);
        FileUtils.saveFile(signsConfig, "signs.yml");
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        FileConfiguration signsConfig = FileUtils.getFile("signs.yml");
        String stringLoc = LocationUtil.serialize(e.getBlock().getLocation());
        if (!signsConfig.isSet("signs"))
            return;
        List<String> signs = signsConfig.getStringList("signs");
        if(signs.contains(stringLoc)) {
            signs.remove(stringLoc);
            signsConfig.set("signs", signs);
            FileUtils.saveFile(signsConfig, "signs.yml");
            e.getPlayer().sendMessage(Message.SIGN_REMOVED.getMsg());
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if(e.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        Block block = e.getClickedBlock();
        if(block.getType() != Material.SIGN && block.getType() != Material.SIGN_POST && block.getType() != Material.WALL_SIGN) {
            return;
        }
        Player p = e.getPlayer();
        FileConfiguration signsConfig = FileUtils.getFile("signs.yml");
        List<String> signs = signsConfig.isSet("signs") ? signsConfig.getStringList("signs") : new ArrayList<>();
        String stringLoc = LocationUtil.serialize(block.getLocation());
        if(!signs.contains(stringLoc))
            return;
        Sign sign = (Sign) block.getState();
        String[] lines = sign.getLines();
        for(int i = 0; i < lines.length; i++)
            lines[i] = Utils.uncolor(lines[i]);
        List<String> playerLines = plugin.getConfig().getStringList("signs." + SignType.JOIN_PLAYER.toString() + ".lines");
        String[] pLines = playerLines.toArray(new String[playerLines.size()]);
        List<String> mobO15Lines = plugin.getConfig().getStringList("signs." + SignType.JOIN_MOB_OVER15.toString() + ".lines");
        String[] mO15Lines = mobO15Lines.toArray(new String[mobO15Lines.size()]);
        List<String> mobU15Lines = plugin.getConfig().getStringList("signs." + SignType.JOIN_MOB_UNDER15.toString() + ".lines");
        String[] mU15Lines = mobU15Lines.toArray(new String[mobU15Lines.size()]);

        if(Arrays.equals(lines, pLines)) {
            if(plugin.getMatchManager().isPlayerQueued()) {
                if(plugin.getMatchManager().getQueued().getName().equals(p.getName())) {
                    p.sendMessage(Message.LEFT_QUEUE.getMsg());
                    plugin.getMatchManager().unqueuePlayer();
                } else {
                    plugin.getMatchManager().createMatch(MatchType.PLAYER, null, p, plugin.getMatchManager().getQueued());
                }
            } else {
                p.sendMessage(Message.QUEUED.getMsg());
                plugin.getMatchManager().queuePlayer(p);
            }
        } else if (Arrays.equals(lines, mO15Lines)) {
            if(plugin.getLevelManager().getBalance(p.getUniqueId()) < 15) {
                p.sendMessage(Message.OVER_15_LEVEL.getMsg());
                return;
            }

            plugin.getMatchManager().createMatch(MatchType.MOB_OVER_15, null, p);
        } else if (Arrays.equals(lines, mU15Lines)) {
            if(plugin.getLevelManager().getBalance(p.getUniqueId()) >= 15) {
                p.sendMessage(Message.UNDER_15_LEVEL.getMsg());
                return;
            }

            plugin.getMatchManager().createMatch(MatchType.MOB_UNDER_15, null, p);
        }
    }
    
}
