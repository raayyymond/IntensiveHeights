package me.abe.intensiveheights.match;

import me.abe.intensiveheights.IntensiveHeights;
import me.abe.intensiveheights.arena.ArenaID;
import me.abe.intensiveheights.equipment.EquipmentType;
import me.abe.intensiveheights.equipment.Setup;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class MatchManager {

    private IntensiveHeights plugin;
    private List<Match> matches;
    private Player queued;

    public MatchManager() {
        plugin = IntensiveHeights.getInstance();
        matches = new ArrayList<>();
    }

    public void createMatch(MatchType type, String data, Player... players) {
        ArenaID rand = plugin.getArenaManager().getRandAvailableArena();
        Match match = null;
        switch (type) {
            case DUEL:
                match = new Match(rand, players[0], players[1], data);
                break;
            case PLAYER:
                match = new Match(rand, players[0], players[1]);
                queued = null;
                break;
            case MOB_OVER_15:
            case MOB_UNDER_15:
                match = new Match(rand, players[0], type);
                spawnMob(match);
        }
        matches.add(match);
        plugin.getArenaManager().useArena(match.getId());
        int i = 1;
        for(Player player : players) {
            loadLoadout(player);
            player.teleport(match.getArena().getLoc(i, rand));
            i++;
        }
    }

    private void loadLoadout(Player player) {
        player.getInventory().clear();
        Setup setup = plugin.getEquipmentManager().getSetup(player);
            for(EquipmentType type : EquipmentType.values()) {
                String[] rawItemData = plugin.getConfig().getString("equipment." + type.toString() + "." + setup.getLevel(type) + ".item").split(", ");
                ItemStack item = new ItemStack(Material.getMaterial(rawItemData[0]), 1, Short.valueOf(rawItemData[1]));
                for(int i = 2; i < rawItemData.length; i++) {
                    String[] rawEnchData = rawItemData[i].split(":");
                    item.addUnsafeEnchantment(Enchantment.getByName(rawEnchData[0]), Integer.valueOf(rawEnchData[1]));
                }

                if(type == EquipmentType.HELMET) {
                    player.getInventory().setHelmet(item);
                } else if(type == EquipmentType.CHESTPLATE) {
                    player.getInventory().setChestplate(item);
                } else if(type == EquipmentType.LEGGINGS) {
                    player.getInventory().setLeggings(item);
                } else if(type == EquipmentType.BOOTS) {
                    player.getInventory().setBoots(item);
                } else if(type == EquipmentType.SWORD) {
                player.getInventory().setItem(0, item);
            } else if(type == EquipmentType.POTION) {
                for(int i = 0; i < player.getInventory().getSize(); i++)
                    if(player.getInventory().getItem(i) == null)
                        player.getInventory().setItem(i, item);
            }
        }
        player.updateInventory();
    }

    private void spawnMob(Match match) {

    }

    public boolean isInMatch(Player player) {
        for(Match match : matches) {
            if(match.getEntity1().getName().equals(player.getName()) || (match.getEntity2() != null && match.getEntity2().getName().equals(player.getName())))
                return true;
        }
        return false;
    }

    public void unqueuePlayer() {
        queued = null;
    }

    public boolean isPlayerQueued() {
        return queued != null;
    }

    public void queuePlayer(Player player) {
        queued = player;
    }

    public Player getQueued() {
        return queued;
    }

}
