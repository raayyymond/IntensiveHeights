package me.abe.intensiveheights.match;

import me.abe.intensiveheights.arena.Arena;
import me.abe.intensiveheights.arena.ArenaID;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Match {

    private Arena arena;
    private ArenaID id;
    private Entity entity1;
    private Entity entity2;
    private MatchType type;
    private String betData;

    public Match(ArenaID id, Player player, MatchType type) {
        arena = Arena.getById(id.getId());
        this.id = id;
        entity1 = player;
        this.type = type;
        betData = null;
    }

    public Match(ArenaID id, Player player1, Player player2) {
        arena = Arena.getById(id.getId());
        this.id = id;
        entity1 = player1;
        entity2 = player2;
        type = MatchType.PLAYER;
        betData = null;
    }

    public Match(ArenaID id, Player player1, Player player2, String betData) {
        arena = Arena.getById(id.getId());
        this.id = id;
        entity1 = player1;
        entity2 = player2;
        type = MatchType.DUEL;
        this.betData = betData;
    }

    public Arena getArena() {
        return arena;
    }

    public ArenaID getId() {
        return id;
    }

    public Entity getEntity1() {
        return entity1;
    }

    public Entity getEntity2() {
        return entity2;
    }

    public MatchType getType() {
        return type;
    }

    public String getBetData() {
        return betData;
    }

}
