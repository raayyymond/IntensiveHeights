package me.abe.intensiveheights.match;

public enum MatchType {

    MOB_OVER_15,
    MOB_UNDER_15,
    PLAYER,
    DUEL;

}
