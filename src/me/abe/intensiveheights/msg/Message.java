package me.abe.intensiveheights.msg;

import me.abe.intensiveheights.util.FileUtils;
import me.abe.intensiveheights.util.Utils;
import org.bukkit.configuration.file.FileConfiguration;

public enum Message {

    MUST_BE_PLAYER("&cYou must be a player to run this command!"),
    TOKEN_BALANCE("&aToken Balance: %bal%"),
    OTHER_TOKEN_BLANACE("&a%player%'s Token Balance: %bal%"),
    TOKEN_CMD_HELP("&c/token (player)"),
    NOT_ONLINE("&cThat player is not online!"),
    NO_PERMISSION("&cYou don't have permission to run this command!"),
    INVALID_NUMBER("&cThat number is not valid!"),
    INSUFFICIENT_FUNDS("&cInsufficient funds!"),
    LEVELED_UP("&aYou have used %amt% tokens to levelup to level %level%!"),
    LEVEL_BALANCE("&aLevel: %level%"),
    OTHER_LEVEL_BALANCE("&a%player%'s Level: %level%"),
    LEVEL_CMD_HELP("&c/level (player)"),
    SIGN_CREATED("&aSign created"),
    SIGN_REMOVED("&aSign removed"),
    ARENA_CMD_HELP("&cCorrect Usage:&n&c/arena create [name]&n&c/arena setspawn [name] [1/2]&n&c/arena setamount [name] [amt]&n&c/arena delete [name]"),
    ARENA_DOESNT_EXIST("&cThat arena doesn't exist!"),
    ARENA_EXISTS("&cThat arena exists already!"),
    ARENA_CREATED("&aThe arena %name% has been created!"),
    ARENA_REMOVED("&aThe arena %name% has been removed!"),
    INVALID_SPAWN_NUMBERS("&cThe spawn number must be either 1 or 2."),
    SET_SPAWN("&aYou have set spawn %num% for arena %name%."),
    SET_AMOUNT("&aYou have set the amount of arenas for arena %name%."),
    UNDER_15_LEVEL("&cYou must be less than level 15 to use this sign!"),
    OVER_15_LEVEL("&cYou must be level 15 or high to use this sign!"),
    QUEUED("&aYou have been queued for a match against a player!"),
    LEFT_QUEUE("&cYou have left the queue for a match against a player!"),
    DUEL_CMD_HELP("&cUsage: /duel [player] [amount] [token/level]"),
    ALREADY_IN_MATCH("&cThat player is already in a match!"),
    ALREADY_IN_QUEUE("&cThat player is in a queue for another match. They must leave that queue."),
    INVALID_BET_TYPE("&c%input% is an invalid bet type! Must be \"token\" or \"level\"."),
    DUEL_SENT("&aYou have challenged %player% for %amt% %type%!"),
    DUEL_RECIEVED("&aYou have received a challenge from %player% for %amt% %type%! Type \"/accept %player%\" to accept and \"/deny %player%\" to deny."),
    DUEL_ALREADY_SENT("&cThat player still has a pending challenge from you!"),
    DUEL_EXPIRED1("&cYour challenge from %player% has expired!"),
    DUEL_EXPIRED2("&cYour challenge to %player% has expired!"),
    ACCEPT_CMD_HELP("&cUsage: /accept [player]"),
    NO_CHALLENGE("&cYou don't have an existing challenge from that player!");


    String msg;
    Message(String string) {
        msg = string;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getMsg() {
        return Utils.color(msg);
    }
    public String getRawMsg() {
        return msg;
    }
    public static void loadMessages() {
        FileConfiguration config = FileUtils.getFile("messages.yml");
        for(Message msg : Message.values()) {
            if(config.contains("messages." + msg.toString()))
                msg.setMsg(config.getString("messages." + msg.toString()));
            else
                config.set("messages." + msg.toString(), msg.getRawMsg());
        }
        FileUtils.saveFile(config, "messages.yml");
    }
    public String toString() {
        return name();
    }

}
