package me.abe.intensiveheights.arena;

import me.abe.intensiveheights.IntensiveHeights;
import me.abe.intensiveheights.util.FileUtils;
import me.abe.intensiveheights.util.LocationUtil;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.*;

public class ArenaManager {

    private IntensiveHeights plugin;
    private Map<Arena, Set<ArenaID>> usedArenas;

    public ArenaManager() {
        plugin = IntensiveHeights.getInstance();
        usedArenas = new HashMap<>();
        loadArenas();
    }

    private void loadArenas() {
        FileConfiguration arenaConfig = FileUtils.getFile("arenas.yml");

        if(arenaConfig.getConfigurationSection("arenas") == null)
            return;

        for(String id : arenaConfig.getConfigurationSection("arenas").getKeys(false)) {
            Location loc1 = LocationUtil.deserializePlayer(arenaConfig.getString("arenas." + id + ".loc1"));
            Location loc2 = LocationUtil.deserializePlayer(arenaConfig.getString("arenas." + id + ".loc2"));
            int amt = arenaConfig.getInt("arenas." + id + ".amt");
            Arena arena = new Arena(loc1, loc2, id, amt);
            usedArenas.put(arena, new HashSet<>());
        }
    }

    public void useArena(ArenaID id) {
        Arena arena = Arena.getById(id.getId());
        Set<ArenaID> ids = usedArenas.get(arena);
        ids.add(id);
        usedArenas.put(arena, ids);
    }

    public void cleanUpArena(ArenaID id) {
        Arena arena = Arena.getById(id.getId());
        Set<ArenaID> nums = usedArenas.get(Arena.getById(id.getId()));
        nums.remove(id);
        usedArenas.put(arena, nums);
    }

    public ArenaID getRandAvailableArena() {
        Random random = new Random();
        List<Arena> openArenas = getOpenArenas();
        if(openArenas.size() == 0)
            return null;
        Arena arena = openArenas.get(random.nextInt(openArenas.size()));
        String id = arena.getId();
        Set<ArenaID> used = usedArenas.get(arena);
        Set<ArenaID> numIds = new HashSet<>();
        for(int i = 1; i <= arena.getAmt(); i++)
            numIds.add(new ArenaID(id, i));
        for(ArenaID aID : used)
            numIds.remove(aID);
        return numIds.toArray(new ArenaID[numIds.size()])[random.nextInt(numIds.size())];
    }

    private List<Arena> getOpenArenas() {
        List<Arena> open = new ArrayList<>();
        for (Arena arena : usedArenas.keySet()) {
            Set<ArenaID> numIds = usedArenas.get(arena);
            boolean free = false;
            for (int i = 1; i <= arena.getAmt(); i++) {
                if (!numIds.contains(new ArenaID(arena.getId(), i))) {
                    free = true;
                    break;
                }
            }
            if (free)
                open.add(arena);
        }
        return open;
    }

    public boolean arenaExists(String id) {
        for(Arena arena : usedArenas.keySet())
            if(arena.getId().equals(id))
                return true;
        return false;
    }

    public void createArena(String id) {
        usedArenas.put(new Arena(id, 0), new HashSet<>());
    }

    public void deleteArena(String id) {
        Arena arena = null;
        for(Arena arena1 : usedArenas.keySet())
            if(arena1.getId().equals(id))
                arena = arena1;
        usedArenas.remove(arena);
        FileConfiguration arenaConfig = FileUtils.getFile("arenas.yml");
        arenaConfig.set("arenas." + id, null);
        FileUtils.saveFile(arenaConfig, "arenas.yml");
    }

    public void setSpawn(String id, int num, Location loc) {
        Arena arena = null;
        for(Arena arena1 : usedArenas.keySet())
            if(arena1.getId().equals(id))
                arena = arena1;
        usedArenas.remove(arena);
        switch (num) {
            case 1:
                arena.setLoc1(loc);
                break;
            case 2:
                arena.setLoc2(loc);
        }
        usedArenas.put(arena, new HashSet<>());
        FileConfiguration arenaConfig = FileUtils.getFile("arenas.yml");
        arenaConfig.set("arenas." + id + ".loc" + num, LocationUtil.serializePlayer(loc));
        FileUtils.saveFile(arenaConfig, "arenas.yml");
    }

    public void setAmount(String id, int amt) {
        Arena arena = null;
        for(Arena arena1 : usedArenas.keySet())
            if(arena1.getId().equals(id))
                arena = arena1;
        usedArenas.remove(arena);
        arena.setAmt(amt);
        usedArenas.put(arena, new HashSet<>());
        FileConfiguration arenaConfig = FileUtils.getFile("arenas.yml");
        arenaConfig.set("arenas." + id + ".amt", amt);
        FileUtils.saveFile(arenaConfig, "arenas.yml");
    }
}
