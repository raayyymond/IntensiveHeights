package me.abe.intensiveheights.arena;

public class ArenaID {

    private String id;
    private int numId;

    public ArenaID(String id, int numId) {
        this.id = id;
        this.numId = numId;
    }

    public String getId() {
        return id;
    }

    public int getNumId() {
        return numId;
    }

    public boolean equals(Object obj) {
        return obj instanceof ArenaID && ((ArenaID) obj).getId().equals(getId()) && ((ArenaID) obj).getNumId() == getNumId();
    }

}
