package me.abe.intensiveheights.arena;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.util.HashMap;
import java.util.Map;

public class Arena {

    private static Map<String, Arena> arenas = new HashMap<>();

    private Location loc1;
    private Location loc2;
    private String id;
    private int amt;

    public Arena(String id, int amt) {
        this.id = id;
        this.amt = amt;
    }

    public Arena(Location loc1, Location loc2, String id, int amt) {
        this.loc1 = loc1;
        this.loc2 = loc2;
        this.id = id;
        this.amt = amt;
        arenas.put(id, this);
    }

    private Location getLoc1(ArenaID id) {
        String worldName = loc1.getWorld().getName();
        worldName = worldName.replaceAll("-\\d+", "");
        worldName = worldName + "-" + id.getNumId();
        World world = new WorldCreator(worldName).createWorld();
        Location loc = loc1;
        loc.setWorld(world);
        return loc;
    }

    public void setLoc1(Location loc1) {
        this.loc1 = loc1;
    }

    private Location getLoc2(ArenaID id) {
        String worldName = loc2.getWorld().getName();
        worldName = worldName.replaceAll("-\\d+", "");
        worldName = worldName + "-" + id.getNumId();
        World world = new WorldCreator(worldName).createWorld();
        Location loc = loc2;
        loc.setWorld(world);
        return loc;
    }

    public Location getLoc(int loc, ArenaID id) {
        switch (loc) {
            case 1:
                return getLoc1(id);
            case 2:
                return getLoc2(id);
        }

        return null;
    }

    public void setLoc2(Location loc2) {
        this.loc2 = loc2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAmt() {
        return amt;
    }

    public void setAmt(int amt) {
        this.amt = amt;
    }

    public static Arena getById(String id) {
        return arenas.get(id);
    }

}
