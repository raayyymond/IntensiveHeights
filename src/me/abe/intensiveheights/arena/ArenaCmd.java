package me.abe.intensiveheights.arena;

import me.abe.intensiveheights.IntensiveHeights;
import me.abe.intensiveheights.msg.Message;
import me.abe.intensiveheights.util.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ArenaCmd implements CommandExecutor {

    private IntensiveHeights plugin;

    public ArenaCmd() {
        plugin = IntensiveHeights.getInstance();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(Message.MUST_BE_PLAYER.getMsg());
            return false;
        }

        Player player = (Player) sender;

        if(!player.hasPermission("ih.arena.admin")) {
            player.sendMessage(Message.NO_PERMISSION.getMsg());
            return false;
        }

        if(args.length == 2) {
            if(args[0].equalsIgnoreCase("create")) {
                if(plugin.getArenaManager().arenaExists(args[1])) {
                    player.sendMessage(Message.ARENA_EXISTS.getMsg());
                    return false;
                }

                plugin.getArenaManager().createArena(args[1]);
                player.sendMessage(Message.ARENA_CREATED.getMsg().replace("%name%", args[1]));
            } else if(args[0].equalsIgnoreCase("delete")) {
                if(!plugin.getArenaManager().arenaExists(args[1])) {
                    player.sendMessage(Message.ARENA_DOESNT_EXIST.getMsg());
                    return false;
                }

                plugin.getArenaManager().deleteArena(args[1]);
                player.sendMessage(Message.ARENA_REMOVED.getMsg().replace("%name%", args[1]));
            } else {
                sendHelp(player);
                return false;
            }
        } else if(args.length == 3) {
            if(args[0].equalsIgnoreCase("setspawn")) {
                if(!plugin.getArenaManager().arenaExists(args[1])) {
                    player.sendMessage(Message.ARENA_DOESNT_EXIST.getMsg());
                    return false;
                }

                if(!args[2].equals("1") && !args[2].equals("2")) {
                    player.sendMessage(Message.INVALID_SPAWN_NUMBERS.getMsg());
                    return false;
                }

                plugin.getArenaManager().setSpawn(args[1], Integer.valueOf(args[2]), player.getLocation());
                player.sendMessage(Message.SET_SPAWN.getMsg().replace("%name%", args[1]).replace("%num%", args[2]));
            } else if(args[0].equalsIgnoreCase("setamount")) {
                if(!plugin.getArenaManager().arenaExists(args[1])) {
                    player.sendMessage(Message.ARENA_DOESNT_EXIST.getMsg());
                    return false;
                }

                int amt;
                try {
                    amt = Integer.valueOf(args[2]);
                } catch (NumberFormatException e) {
                    player.sendMessage(Message.INVALID_NUMBER.getMsg());
                    return false;
                }

                plugin.getArenaManager().setAmount(args[1], amt);
                player.sendMessage(Message.SET_AMOUNT.getMsg().replace("%name%", args[1]));
            } else {
                sendHelp(player);
                return false;
            }
        } else {
            sendHelp(player);
            return false;
        }


        return true;
    }
    
    private void sendHelp(Player player) {
        for(String raw : Message.ARENA_CMD_HELP.getRawMsg().split("&n")) {
            player.sendMessage(Utils.color(raw));
        }
    }
}
