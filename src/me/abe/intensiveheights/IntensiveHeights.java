package me.abe.intensiveheights;

import me.abe.intensiveheights.arena.ArenaCmd;
import me.abe.intensiveheights.arena.ArenaManager;
import me.abe.intensiveheights.duel.DuelCmd;
import me.abe.intensiveheights.equipment.EquipmentCmd;
import me.abe.intensiveheights.equipment.EquipmentGUI;
import me.abe.intensiveheights.equipment.EquipmentManager;
import me.abe.intensiveheights.level.LevelCmd;
import me.abe.intensiveheights.level.LevelManager;
import me.abe.intensiveheights.level.LevelUpCmd;
import me.abe.intensiveheights.match.MatchManager;
import me.abe.intensiveheights.sign.SignListener;
import me.abe.intensiveheights.token.TokenCmd;
import me.abe.intensiveheights.token.TokenManager;
import me.abe.intensiveheights.msg.Message;
import me.abe.intensiveheights.util.FileUtils;
import org.bukkit.plugin.java.JavaPlugin;

public class IntensiveHeights extends JavaPlugin {

    private static IntensiveHeights INSTANCE;

    private TokenManager tokenManager;
    private LevelManager levelManager;
    private EquipmentManager equipmentManager;
    private EquipmentGUI equipmentGUI;
    private ArenaManager arenaManager;
    private MatchManager matchManager;

    public void onEnable() {
        INSTANCE = this;

        saveDefaultConfig();
        Message.loadMessages();
        saveDefault("arenas.yml");
        saveDefault("tokens.yml");
        saveDefault("equipment.yml");
        saveDefault("levels.yml");
        saveDefault("signs.yml");

        tokenManager = new TokenManager();
        levelManager = new LevelManager();
        equipmentManager = new EquipmentManager();
        equipmentGUI = new EquipmentGUI();
        new SignListener();
        arenaManager = new ArenaManager();
        matchManager = new MatchManager();

        registerCommands();

        getLogger().info(getDescription().getName() + " v" + getDescription().getVersion() + " has been successfully been enabled!");
    }

    public TokenManager getTokenManager() {
        return tokenManager;
    }

    public LevelManager getLevelManager() {
        return levelManager;
    }

    public EquipmentManager getEquipmentManager() {
        return equipmentManager;
    }

    public EquipmentGUI getEquipmentGUI() {
        return equipmentGUI;
    }

    public ArenaManager getArenaManager() {
        return arenaManager;
    }

    public MatchManager getMatchManager() {
        return matchManager;
    }

    public static IntensiveHeights getInstance() {
        return INSTANCE;
    }

    private void registerCommands() {
        getCommand("token").setExecutor(new TokenCmd());
        getCommand("level").setExecutor(new LevelCmd());
        getCommand("levelup").setExecutor(new LevelUpCmd());
        getCommand("upgrade").setExecutor(new EquipmentCmd());
        getCommand("arena").setExecutor(new ArenaCmd());
        getCommand("duel").setExecutor(new DuelCmd());
        getCommand("accept").setExecutor(getCommand("duel").getExecutor());
    }

    private void saveDefault(String file) {
        if(!FileUtils.getFile(file, false).exists())
            saveResource(file, false);
    }
    
}
