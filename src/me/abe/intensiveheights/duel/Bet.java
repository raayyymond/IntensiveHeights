package me.abe.intensiveheights.duel;

import org.bukkit.entity.Player;

public class Bet {

    private BetType type;
    private int bet;
    private Player player;

    public Bet(BetType type, int bet, Player player) {
        this.type = type;
        this.bet = bet;
        this.player = player;
    }

    public BetType getType() {
        return type;
    }

    public int getBet() {
        return bet;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Bet) {
            Bet bet = (Bet) obj;
            return bet.getPlayer().getName().equals(getPlayer().getName());
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return type.toString() + " " + bet;
    }
}
