package me.abe.intensiveheights.duel;

import me.abe.intensiveheights.IntensiveHeights;
import me.abe.intensiveheights.match.MatchType;
import me.abe.intensiveheights.msg.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

public class DuelCmd implements CommandExecutor {

    private IntensiveHeights plugin;
    private Map<UUID, List<Bet>> pending;

    public DuelCmd() {
        plugin = IntensiveHeights.getInstance();
        pending = new HashMap<>();

        if(!plugin.getConfig().isSet("duel.timeout"))
            plugin.getConfig().set("duel.timeout", 30);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(Message.MUST_BE_PLAYER.getMsg());
            return false;
        }

        Player player = (Player) sender;

        if(cmd.getName().equalsIgnoreCase("duel")) {
            if (args.length == 3) {
                Player other = Bukkit.getPlayer(args[0]);
                if (other == null) {
                    player.sendMessage(Message.NOT_ONLINE.getMsg());
                    return false;
                }

                if (plugin.getMatchManager().isInMatch(other)) {
                    player.sendMessage(Message.ALREADY_IN_MATCH.getMsg());
                    return false;
                }

                if (plugin.getMatchManager().getQueued().getName().equals(other.getName())) {
                    player.sendMessage(Message.ALREADY_IN_QUEUE.getMsg());
                    return false;
                }

                for (Bet bet : pending.get(other.getUniqueId())) {
                    if (bet.getPlayer().getName().equals(player.getName())) {
                        player.sendMessage(Message.DUEL_ALREADY_SENT.getMsg());
                        return false;
                    }
                }

                int bet;
                try {
                    bet = Integer.valueOf(args[1]);
                } catch (NumberFormatException e) {
                    player.sendMessage(Message.INVALID_NUMBER.getMsg());
                    return false;
                }

                if (!(args[2].equalsIgnoreCase("token") || args[2].equalsIgnoreCase("level"))) {
                    player.sendMessage(Message.INVALID_BET_TYPE.getMsg().replace("%input%", args[2]));
                    return false;
                }

                player.sendMessage(Message.DUEL_SENT.getMsg()
                        .replace("%player%", other.getName())
                        .replace("%amt%", bet + "")
                        .replace("%type%", args[2].toLowerCase() + "s"));
                other.sendMessage(Message.DUEL_RECIEVED.getMsg()
                        .replace("%player%", player.getName())
                        .replace("%amt%", bet + "")
                        .replace("%type%", args[2].toLowerCase() + "s"));
                List<Bet> bets = pending.getOrDefault(other.getUniqueId(), new ArrayList<>());
                Bet bet1 = new Bet(BetType.valueOf(args[2].toUpperCase()), bet, player);
                bets.add(bet1);
                pending.put(other.getUniqueId(), bets);

                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                    List<Bet> bets2 = pending.getOrDefault(other.getUniqueId(), new ArrayList<>());
                    boolean contains = false;
                    for (Bet bet2 : bets2) {
                        if (bet2.equals(bet1)) {
                            contains = true;
                            break;
                        }
                    }
                    if (contains) {
                        bets2.remove(bet1);
                        player.sendMessage(Message.DUEL_EXPIRED2.getMsg().replace("%player%", other.getName()));
                        other.sendMessage(Message.DUEL_EXPIRED1.getMsg().replace("%player%", player.getName()));
                    }
                    pending.put(other.getUniqueId(), bets2);
                }, plugin.getConfig().getInt("duel.timeout") * 20);
            } else {
                player.sendMessage(Message.DUEL_CMD_HELP.getMsg());
            }
        } else if(cmd.getName().equalsIgnoreCase("accept")) {
            if(args.length == 1) {
                Player other = Bukkit.getPlayer(args[0]);
                if(other == null) {
                    player.sendMessage(Message.NOT_ONLINE.getMsg());
                    return false;
                }

                if(pending.get(player.getUniqueId()) == null) {
                    player.sendMessage(Message.NO_CHALLENGE.getMsg());
                    return false;
                }

                List<Bet> bets = pending.get(player.getUniqueId());
                if(!bets.contains(new Bet(null, 0, other))) {
                    player.sendMessage(Message.NO_CHALLENGE.getMsg());
                    return false;
                }

                if(plugin.getMatchManager().isInMatch(other)) {
                    player.sendMessage(Message.ALREADY_IN_MATCH.getMsg());
                    return false;
                }

                if(plugin.getMatchManager().isPlayerQueued()) {
                    player.sendMessage(Message.ALREADY_IN_QUEUE.getMsg());
                    return false;
                }

                Bet bet = null;
                for(Bet bet1 : bets) {
                    if(bet1.getPlayer().getName().equals(other.getName())) {
                        bet = bet1;
                        break;
                    }
                }

                bets.remove(bet);
                pending.put(other.getUniqueId(), new ArrayList<>());
                pending.put(player.getUniqueId(), new ArrayList<>());

                plugin.getMatchManager().createMatch(MatchType.DUEL, bet.toString(), player, other);
            } else {
                player.sendMessage(Message.ARENA_CMD_HELP.getMsg());
            }
        }

        return true;
    }
}
