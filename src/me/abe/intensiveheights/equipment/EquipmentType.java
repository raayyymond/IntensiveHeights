package me.abe.intensiveheights.equipment;

public enum EquipmentType {

    HELMET, CHESTPLATE, LEGGINGS, BOOTS, SWORD, POTION

}
