package me.abe.intensiveheights.equipment;

import me.abe.intensiveheights.IntensiveHeights;
import me.abe.intensiveheights.msg.Message;
import me.abe.intensiveheights.util.ItemStackUtil;
import me.abe.intensiveheights.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class EquipmentGUI implements Listener {

    private IntensiveHeights plugin;
    private List<Inventory> invs;

    public EquipmentGUI() {
        plugin = IntensiveHeights.getInstance();
        invs = new ArrayList<>();
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    public void show(Player player) {
        Inventory inv = Bukkit.createInventory(null, 54, "Equipment");
        Setup setup = plugin.getEquipmentManager().getSetup(player);
        int i = 0;
        for(EquipmentType type : EquipmentType.values()) {
            int maxLevel = plugin.getConfig().getInt("equipment." + type.toString() + ".max-level");
            for(int j = 0; j < maxLevel; j++) {
                ItemStack blackPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
                ItemMeta blackPaneMeta = blackPane.getItemMeta();
                blackPaneMeta.setDisplayName(" ");
                blackPane.setItemMeta(blackPaneMeta);
                inv.setItem((i * 9) + 1 + j, blackPane);
            }

            int level = 0;
            switch (type) {
                case HELMET:
                    level = setup.getHelmet();
                    break;
                case CHESTPLATE:
                    level = setup.getChestplate();
                    break;
                case LEGGINGS:
                    level = setup.getLeggings();
                    break;
                case BOOTS:
                    level = setup.getBoots();
                    break;
                case SWORD:
                    level = setup.getSword();
                    break;
                case POTION:
                    level = setup.getPotion();
                    break;
            }

            ItemStack item = ItemStackUtil.deserialize(plugin.getConfig().getString("equipment." + type.toString() + ".levels." + level + ".item"));
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(Utils.color(plugin.getConfig().getString("gui." + type.toString().toLowerCase() + ".name")).replace("%level%", level + ""));
            item.setItemMeta(meta);
            inv.setItem(i * 9, item);

            for(int j = 0; j < level; j++) {
                ItemStack greenPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
                ItemMeta greenPaneItemMeta = greenPane.getItemMeta();
                greenPaneItemMeta.setDisplayName(" ");
                greenPane.setItemMeta(greenPaneItemMeta);
                inv.setItem((i * 9) + 1 + j, greenPane);
            }

            if(level < maxLevel) {
                ItemStack upgradeItem = new ItemStack(Material.getMaterial(plugin.getConfig().getInt("gui.upgrade-item.material-id")));
                ItemMeta upgradeMeta = upgradeItem.getItemMeta();
                upgradeMeta.setDisplayName(Utils.color(plugin.getConfig().getString("gui.upgrade-item.name")));
                int finalLevel = level + 1;
                upgradeMeta.setLore(new ArrayList<String>() {{
                    add(Utils.color(Utils.color(plugin.getConfig().getString("gui.lore")).replace("%token%", plugin.getConfig().getInt("equipment." + type.toString() + ".levels." + finalLevel + ".price") + "")));
                }});
                upgradeItem.setItemMeta(upgradeMeta);
                inv.setItem((i * 9) + 8, upgradeItem);
            }
            i++;
        }

        player.openInventory(inv);
        invs.add(inv);
        player.playSound(player.getLocation(), Sound.CHEST_OPEN, 1, 0);
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if(!invs.contains(e.getClickedInventory()))
            return;

        e.setCancelled(true);

        Player player = (Player) e.getWhoClicked();
        Inventory inv = e.getClickedInventory();
        int ogSlot = e.getSlot();
        int row = ogSlot / 9;
        int col = ogSlot % 9;
        EquipmentType type = EquipmentType.values()[row];
        Setup setup = plugin.getEquipmentManager().getSetup(player);

        if(inv.getItem(ogSlot) == null) {
            return;
        }

        if(col == 8) {
            int level = 0;
            switch (type) {
                case HELMET:
                    level = setup.getHelmet();
                    break;
                case CHESTPLATE:
                    level = setup.getChestplate();
                    break;
                case LEGGINGS:
                    level = setup.getLeggings();
                    break;
                case BOOTS:
                    level = setup.getBoots();
                    break;
                case SWORD:
                    level = setup.getSword();
                    break;
                case POTION:
                    level = setup.getPotion();
                    break;
            }

            level += 1;
            int price = plugin.getConfig().getInt("equipment." + type.toString() + ".levels." + level + ".price");
            int maxLevel = plugin.getConfig().getInt("equipment." + type.toString() + ".max-level");

            if(plugin.getTokenManager().hasEnough(player.getUniqueId(), price)) {
                plugin.getTokenManager().withdraw(player.getUniqueId(), price);
                plugin.getEquipmentManager().upgrade(player, type);
                player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 0);
                for(int j = 0; j < maxLevel; j++) {
                    ItemStack blackPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
                    ItemMeta blackPaneMeta = blackPane.getItemMeta();
                    blackPaneMeta.setDisplayName(" ");
                    blackPane.setItemMeta(blackPaneMeta);
                    inv.setItem((row * 9) + 1 + j, blackPane);
                }
                for(int j = 0; j < level; j++) {
                    ItemStack greenPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
                    ItemMeta greenPaneItemMeta = greenPane.getItemMeta();
                    greenPaneItemMeta.setDisplayName(" ");
                    greenPane.setItemMeta(greenPaneItemMeta);
                    inv.setItem((row * 9) + 1 + j, greenPane);
                }

                ItemStack item = ItemStackUtil.deserialize(plugin.getConfig().getString("equipment." + type.toString() + ".levels." + level + ".item"));
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName(Utils.color(plugin.getConfig().getString("gui." + type.toString().toLowerCase() + ".name")).replace("%level%", level + ""));
                item.setItemMeta(meta);
                inv.setItem(row * 9, item);

                if(level == maxLevel)
                    inv.setItem(row * 9 + 8, null);
                else {
                    ItemStack upgradeItem = inv.getItem(row * 9 + 8);
                    ItemMeta upgradeMeta = upgradeItem.getItemMeta();
                    int finalLevel = level + 1;
                    upgradeMeta.setLore(new ArrayList<String>() {{
                        add(Utils.color(Utils.color(plugin.getConfig().getString("gui.lore")).replace("%token%", plugin.getConfig().getInt("equipment." + type.toString() + ".levels." + finalLevel + ".price") + "")));
                    }});
                    upgradeItem.setItemMeta(upgradeMeta);
                    inv.setItem(row * 9 + 8, upgradeItem);
                }

                player.updateInventory();
            } else {
                player.sendMessage(Message.INSUFFICIENT_FUNDS.getMsg());
                player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 1, 0);
                player.closeInventory();
            }
        }

        invs.remove(e.getClickedInventory());
        invs.add(inv);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if(invs.contains(e.getInventory())) {
            invs.remove(e.getInventory());
            ((Player) e.getPlayer()).playSound(e.getPlayer().getLocation(), Sound.CHEST_CLOSE, 1, 0);
        }
    }

}
