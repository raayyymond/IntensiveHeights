package me.abe.intensiveheights.equipment;

import me.abe.intensiveheights.IntensiveHeights;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EquipmentCmd implements CommandExecutor {

    private IntensiveHeights plugin;

    public EquipmentCmd() {
        plugin = IntensiveHeights.getInstance();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        if(!(sender instanceof Player)) {
            Bukkit.getLogger().info("Command must be run by a player!");
            return false;
        }

        Player player = (Player) sender;

        plugin.getEquipmentGUI().show(player);

        return true;
    }
}
