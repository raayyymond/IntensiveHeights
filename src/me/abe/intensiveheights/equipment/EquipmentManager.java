package me.abe.intensiveheights.equipment;

import me.abe.intensiveheights.IntensiveHeights;
import me.abe.intensiveheights.util.FileUtils;
import me.abe.intensiveheights.util.ItemStackUtil;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EquipmentManager {

    private IntensiveHeights plugin;
    private Map<UUID, Setup> setups;

    public EquipmentManager() {
        plugin = IntensiveHeights.getInstance();
        loadDefaultConfig();
        loadSetups();
    }

    private void loadDefaultConfig() {
        for(EquipmentType type : EquipmentType.values()) {
            String rawType = type.toString();
            if(!plugin.getConfig().isConfigurationSection("equipment." + rawType)) {
                plugin.getConfig().set("equipment." + rawType + ".max-level", 7);
            }
            for(int i = 1; i <= plugin.getConfig().getInt("equipment." + rawType + ".max-level"); i++) {
                if(plugin.getConfig().isConfigurationSection("equipment." + rawType + ".levels." + i))
                    continue;

                if(type == EquipmentType.POTION) {
                    Potion potion = new Potion(PotionType.INSTANT_HEAL);
                    potion.setSplash(true);
                    potion.setLevel(1);
                    ItemStack item = new ItemStack(Material.POTION);
                    potion.apply(item);
                    plugin.getConfig().set("equipment." + rawType + ".levels." + i + ".item", ItemStackUtil.serialize(item));
                } else {
                    ItemStack item = new ItemStack(Material.getMaterial("DIAMOND_" + rawType));
                    if(type == EquipmentType.SWORD) {
                        item.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, i);
                    } else {
                        item.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, i);
                    }
                    plugin.getConfig().set("equipment." + rawType + ".levels." + i + ".item", ItemStackUtil.serialize(item));
                }

                plugin.getConfig().set("equipment." + rawType + ".levels." + i + ".price", i - 1);
            }
        }
        plugin.saveConfig();
    }
    
    private void loadSetups() {
        setups = new HashMap<>();
        FileConfiguration setupConfig = FileUtils.getFile("equipment.yml");
        for(String rawUUID : setupConfig.getConfigurationSection("").getKeys(false)) {
            UUID uuid = UUID.fromString(rawUUID);
            int helmet = setupConfig.getInt(rawUUID + ".helmet");
            int chestplate = setupConfig.getInt(rawUUID + ".chestplate");
            int leggings = setupConfig.getInt(rawUUID + ".leggings");
            int boots = setupConfig.getInt(rawUUID + ".boots");
            int sword = setupConfig.getInt(rawUUID + ".sword");
            int potion = setupConfig.getInt(rawUUID + ".potion");
            Setup setup = new Setup(helmet, chestplate, leggings, boots, sword, potion);
            setups.put(uuid, setup);
        }
    }

    public Setup getSetup(Player player) {
        if(!hasSetup(player)) {
            Setup setup = new Setup(1, 1, 1, 1, 1, 1);
            setups.put(player.getUniqueId(), setup);
            updateFile();
        }
        return setups.get(player.getUniqueId());
    }

    public boolean hasSetup(Player player) {
        return setups.containsKey(player.getUniqueId());
    }

    public void upgrade(Player player, EquipmentType type) {
        Setup setup = getSetup(player);
        switch (type) {
            case HELMET:
                setup.setHelmet(setup.getHelmet() + 1);
                break;
            case CHESTPLATE:
                setup.setChestplate(setup.getChestplate() + 1);
                break;
            case LEGGINGS:
                setup.setLeggings(setup.getLeggings() + 1);
                break;
            case BOOTS:
                setup.setBoots(setup.getBoots() + 1);
                break;
            case SWORD:
                setup.setSword(setup.getSword() + 1);
                break;
            case POTION:
                setup.setPotion(setup.getPotion() + 1);
                break;
        }
        setups.put(player.getUniqueId(), setup);
        updateFile();
    }

    private void updateFile() {
        FileConfiguration setupsConfig = FileUtils.getFile("equipment.yml");
        for(UUID uuid : setups.keySet()) {
            String rawUUID = uuid.toString();
            Setup setup = setups.get(uuid);
            int[] levels = setup.getLevels();
            setupsConfig.set(rawUUID + ".helmet", levels[0]);
            setupsConfig.set(rawUUID + ".chestplate", levels[1]);
            setupsConfig.set(rawUUID + ".leggings", levels[2]);
            setupsConfig.set(rawUUID + ".boots", levels[3]);
            setupsConfig.set(rawUUID + ".sword", levels[4]);
            setupsConfig.set(rawUUID + ".potion", levels[5]);
        }
        FileUtils.saveFile(setupsConfig, "equipment.yml");
    }

}
