package me.abe.intensiveheights.equipment;

public class Setup {
    
    private int helmet;
    private int chestplate;
    private int leggings;
    private int boots;
    private int sword;
    private int potion;

    public Setup(int helmet, int chestplate, int leggings, int boots, int sword, int potion) {
        this.helmet = helmet;
        this.chestplate = chestplate;
        this.leggings = leggings;
        this.boots = boots;
        this.sword = sword;
        this.potion = potion;
    }

    public int getHelmet() {
        return helmet;
    }

    public void setHelmet(int helmet) {
        this.helmet = helmet;
    }

    public int getChestplate() {
        return chestplate;
    }

    public void setChestplate(int chestplate) {
        this.chestplate = chestplate;
    }

    public int getLeggings() {
        return leggings;
    }

    public void setLeggings(int leggings) {
        this.leggings = leggings;
    }

    public int getBoots() {
        return boots;
    }

    public void setBoots(int boots) {
        this.boots = boots;
    }

    public int getSword() {
        return sword;
    }

    public void setSword(int sword) {
        this.sword = sword;
    }

    public int getPotion() {
        return potion;
    }

    public void setPotion(int potion) {
        this.potion = potion;
    }

    public int getLevel(EquipmentType type) {
        switch (type) {
            case HELMET:
                return getHelmet();
            case CHESTPLATE:
                return getChestplate();
            case LEGGINGS:
                return getLeggings();
            case BOOTS:
                return getBoots();
            case SWORD:
                return getSword();
            case POTION:
                return getPotion();
        }
        return -1;
    }

    public int[] getLevels() {
        return new int[] {getHelmet(), getChestplate(), getLeggings(), getBoots(), getSword(), getPotion()};
    }
    
}
