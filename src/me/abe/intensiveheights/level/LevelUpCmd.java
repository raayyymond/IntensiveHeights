package me.abe.intensiveheights.level;

import me.abe.intensiveheights.IntensiveHeights;
import me.abe.intensiveheights.msg.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LevelUpCmd implements CommandExecutor {

    private IntensiveHeights plugin;

    public LevelUpCmd() {
        plugin = IntensiveHeights.getInstance();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(Message.MUST_BE_PLAYER.getMsg());
            return false;
        }

        Player player = (Player) sender;
        int currentlevel = plugin.getLevelManager().getBalance(player.getUniqueId());
        int price;

        if(currentlevel < 15)
            price = 10;
        else if(currentlevel <= 25)
            price = 15;
        else
            price = 20;

        if(!plugin.getTokenManager().hasEnough(player.getUniqueId(), price)) {
            player.sendMessage(Message.INSUFFICIENT_FUNDS.getMsg());
            return false;
        }

        plugin.getTokenManager().withdraw(player.getUniqueId(), price);
        plugin.getLevelManager().deposit(player.getUniqueId(), 1);
        player.sendMessage(Message.LEVELED_UP.getMsg().replace("%amt%", price + "").replace("%level%", plugin.getTokenManager().getBalance(player.getUniqueId()) + ""));
        return true;
    }
}
